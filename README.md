# Helm Charts

- [Marty Media Server](https://www.marty-media.org/server/setup/installation.html#helm)

## Release new Chart

### Prerequisites

- [Helm Schema](https://github.com/dadav/helm-schema)
- [Helm Docs](https://github.com/norwoodj/helm-docs)

### Checklist

- Increase Version Number of `Chart.yaml`
- Update `CHANGELOG.md`

### Build new Chart

```
helm-schema
helm-docs --chart-search-root=charts --template-files=../README.md.gotmpl --sort-values-order=file
helm package --sign --key 'martin.riedl.92@googlemail.com' --keyring ~/.gnupg/secring.gpg --destination "./public" charts/marty-media-server
```
