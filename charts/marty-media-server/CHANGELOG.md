# Changelog

## 0.8.x

- NEW: Server Release 0.6.x

## 0.7.x

- NEW: Variable `database.postgresqlcnpg.cluster.backup` for PostgreSQL CNPG cluster backup

## 0.6.x

- CHANGE: use new container registry `registry.marty-media.org` for images

## 0.5.x

- NEW: Configuration for PostgreSQL database (see chapter `database`)

## 0.4.x

- NEW: Variable `config.storage.tempDir` to override the temporary directory
- NEW: Variable `config.storage.cacheSize` to set a target size for the local cache
- REMOVE: ConfigMap for deployment (was not used)
- REMOVE: Parameter `config.webServer.listen.port` (always fixed `43200`)

## 0.3.x

- NEW: Charts are now signed

## 0.2.x

- NEW: `values.schema.yaml` is now included for Chart Values Validation
- CHANGE: `nodeSelector`, `affinity` and `tolerations` are now part of the `deployment` section
