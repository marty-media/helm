{
  "additionalProperties": false,
  "properties": {
    "autoscaling": {
      "additionalProperties": false,
      "properties": {
        "enabled": {
          "default": false,
          "description": "Enables Horizontal Pod Autoscaling. HorizontalPodAutoscaler automatically manages the replica count of any resource implementing the scale subresource based on the metrics specified.\n@section -- Autoscaling",
          "title": "enabled",
          "type": "boolean"
        },
        "maxReplicas": {
          "default": 100,
          "description": "The upper limit for the number of replicas to which the autoscaler can scale up. It cannot be less that minReplicas.\nSee [maxReplicas in HorizontalPodAutoscalerSpec of HorizontalPodAutoscaler](https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/horizontal-pod-autoscaler-v2/#HorizontalPodAutoscalerSpec)\n@section -- Autoscaling",
          "title": "maxReplicas",
          "type": "integer"
        },
        "metrics": {
          "items": {},
          "description": "Contains the specifications for which to use to calculate the desired replica count.\nSee [metrics in HorizontalPodAutoscalerSpec of HorizontalPodAutoscaler](https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/horizontal-pod-autoscaler-v2/#HorizontalPodAutoscalerSpec)\n@section -- Autoscaling",
          "title": "metrics",
          "type": "array"
        },
        "minReplicas": {
          "default": 1,
          "description": "The lower limit for the number of replicas to which the autoscaler can scale down.\nSee [minReplicas in HorizontalPodAutoscalerSpec of HorizontalPodAutoscaler](https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/horizontal-pod-autoscaler-v2/#HorizontalPodAutoscalerSpec)\n@section -- Autoscaling",
          "title": "minReplicas",
          "type": "integer"
        }
      },
      "title": "autoscaling",
      "type": "object",
      "required": [
        "enabled",
        "minReplicas",
        "maxReplicas",
        "metrics"
      ]
    },
    "config": {
      "additionalProperties": false,
      "properties": {
        "storage": {
          "additionalProperties": false,
          "properties": {
            "cacheSize": {
              "default": 200000000,
              "description": "Size (in bytes) for the local file cache.\nSee [Storage in Configuration](https://www.marty-media.org/server/setup/configuration.html#storage)\n@section -- Configuration",
              "title": "cacheSize",
              "type": "integer"
            },
            "tempDir": {
              "default": "",
              "description": "Override the default directory for temporary files.\nSee [Storage in Configuration](https://www.marty-media.org/server/setup/configuration.html#storage)\n@section -- Configuration",
              "title": "tempDir",
              "type": "string"
            }
          },
          "title": "storage",
          "type": "object",
          "required": [
            "tempDir",
            "cacheSize"
          ]
        }
      },
      "title": "config",
      "type": "object",
      "required": [
        "storage"
      ]
    },
    "database": {
      "additionalProperties": false,
      "properties": {
        "postgresMaxOpenConnections": {
          "default": 8,
          "description": "Maximum of open database connection from each deployed instance.\n@section -- Database",
          "title": "postgresMaxOpenConnections",
          "type": "integer"
        },
        "postgresqlcnpg": {
          "additionalProperties": false,
          "properties": {
            "cluster": {
              "additionalProperties": false,
              "properties": {
                "backup": {
                  "additionalProperties": true,
                  "description": "The configuration to be used for backups.\nSee [BackupConfiguration of CloudNativePG](https://cloudnative-pg.io/documentation/1.24/cloudnative-pg.v1/#postgresql-cnpg-io-v1-BackupConfiguration)\n@section -- Database",
                  "title": "backup"
                },
                "instances": {
                  "default": 1,
                  "description": "Number of instances required in the cluster.\nSee [ClusterSpec of CloudNativePG](https://cloudnative-pg.io/documentation/1.24/cloudnative-pg.v1/#postgresql-cnpg-io-v1-ClusterSpec)\n@section -- Database",
                  "title": "instances",
                  "type": "integer"
                },
                "storage": {
                  "additionalProperties": false,
                  "properties": {
                    "size": {
                      "default": "",
                      "description": "Size of the storage. Required if not already specified in the PVC template.\nChanges to this field are automatically reapplied to the created PVCs. Size cannot be decreased.\nSee [StorageConfiguration of CloudNativePG](https://cloudnative-pg.io/documentation/1.24/cloudnative-pg.v1/#postgresql-cnpg-io-v1-StorageConfiguration)\n@section -- Database",
                      "title": "size",
                      "type": "string"
                    }
                  },
                  "title": "storage",
                  "type": "object",
                  "required": [
                    "size"
                  ]
                }
              },
              "title": "cluster",
              "type": "object",
              "required": [
                "instances",
                "storage"
              ]
            }
          },
          "title": "postgresqlcnpg",
          "type": "object",
          "required": [
            "cluster"
          ]
        },
        "postgresqlcredentials": {
          "additionalProperties": false,
          "properties": {
            "address": {
              "default": "",
              "description": "Secret name that contains the address (host name) of the PostgreSQL database.\nSee [Database in Configuration](https://www.marty-media.org/server/setup/configuration.html#database)\n@section -- Database",
              "title": "address",
              "type": "string"
            },
            "addressKey": {
              "default": "host",
              "description": "Key of the secret for the address (host name) of the PostgreSQL database.\nSee [Database in Configuration](https://www.marty-media.org/server/setup/configuration.html#database)\n@section -- Database",
              "title": "addressKey",
              "type": "string"
            },
            "database": {
              "default": "",
              "description": "Secret name that contains the database name of the PostgreSQL database.\nSee [Database in Configuration](https://www.marty-media.org/server/setup/configuration.html#database)\n@section -- Database",
              "title": "database",
              "type": "string"
            },
            "databaseKey": {
              "default": "dbname",
              "description": "Key of the secret for the database name of the PostgreSQL database.\nSee [Database in Configuration](https://www.marty-media.org/server/setup/configuration.html#database)\n@section -- Database",
              "title": "databaseKey",
              "type": "string"
            },
            "password": {
              "default": "",
              "description": "Secret name that contains the password of the PostgreSQL database.\nSee [Database in Configuration](https://www.marty-media.org/server/setup/configuration.html#database)\n@section -- Database",
              "title": "password",
              "type": "string"
            },
            "passwordKey": {
              "default": "password",
              "description": "Key of the secret for the password of the PostgreSQL database.\nSee [Database in Configuration](https://www.marty-media.org/server/setup/configuration.html#database)\n@section -- Database",
              "title": "passwordKey",
              "type": "string"
            },
            "port": {
              "default": "",
              "description": "Secret name that contains the port number of the PostgreSQL database.\nSee [Database in Configuration](https://www.marty-media.org/server/setup/configuration.html#database)\n@section -- Database",
              "title": "port",
              "type": "string"
            },
            "portKey": {
              "default": "port",
              "description": "Key of the secret for the port number of the PostgreSQL database.\nSee [Database in Configuration](https://www.marty-media.org/server/setup/configuration.html#database)\n@section -- Database",
              "title": "portKey",
              "type": "string"
            },
            "user": {
              "default": "",
              "description": "Secret name that contains the username of the PostgreSQL database.\nSee [Database in Configuration](https://www.marty-media.org/server/setup/configuration.html#database)\n@section -- Database",
              "title": "user",
              "type": "string"
            },
            "userKey": {
              "default": "user",
              "description": "Key of the secret for the username of the PostgreSQL database.\nSee [Database in Configuration](https://www.marty-media.org/server/setup/configuration.html#database)\n@section -- Database",
              "title": "userKey",
              "type": "string"
            }
          },
          "title": "postgresqlcredentials",
          "type": "object",
          "required": [
            "address",
            "addressKey",
            "port",
            "portKey",
            "user",
            "userKey",
            "password",
            "passwordKey",
            "database",
            "databaseKey"
          ]
        },
        "type": {
          "default": "",
          "description": "Type of database deployment.\nUse `postgresqlcredentials` and provide login credentials using an existing secret to an already running database.\nUse `postgresqlcnpg` to deploy a Cloud Native PostgreSQL cluster configuration to provision a new PostgreSQL cluster (requires already deployed operator, see https://cloudnative-pg.io).\n@section -- Database",
          "title": "type",
          "enum": [
            "postgresqlcredentials",
            "postgresqlcnpg"
          ]
        }
      },
      "title": "database",
      "type": "object",
      "required": [
        "postgresMaxOpenConnections",
        "postgresqlcredentials",
        "postgresqlcnpg"
      ]
    },
    "deployment": {
      "additionalProperties": false,
      "properties": {
        "affinity": {
          "additionalProperties": true,
          "description": "If specified, the pod's scheduling constraints. Affinity is a group of affinity scheduling rules.\nSee [affinity in PodSpec of Pod](https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/#scheduling)\n@section -- Deployment",
          "title": "affinity"
        },
        "image": {
          "additionalProperties": false,
          "properties": {
            "pullPolicy": {
              "default": "IfNotPresent",
              "description": "Image pull policy. One of Always, Never, IfNotPresent.\nSee [imagePullPolicy in Image of Pod](https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/#image)\n@section -- Deployment",
              "title": "pullPolicy",
              "type": "string"
            },
            "pullSecrets": {
              "items": {},
              "description": "ImagePullSecrets is an optional list of references to secrets in the same namespace to use for pulling any of the images used by this PodSpec.\nSee [imagePullSecrets in PodSpec of Pod](https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/#containers)\n@section -- Deployment",
              "title": "pullSecrets",
              "type": "array"
            },
            "repository": {
              "default": "registry.marty-media.org/server/release",
              "description": "Container image name.\nMore Info: [Container Images](https://kubernetes.io/docs/concepts/containers/images/)\n@section -- Deployment",
              "title": "repository",
              "type": "string"
            },
            "tag": {
              "default": "",
              "description": "Overrides the image tag whose default is the chart appVersion.\nMore Info: [Container Images](https://kubernetes.io/docs/concepts/containers/images/)\n@section -- Deployment",
              "title": "tag",
              "type": "string"
            }
          },
          "title": "image",
          "type": "object",
          "required": [
            "repository",
            "tag",
            "pullPolicy",
            "pullSecrets"
          ]
        },
        "nodeSelector": {
          "additionalProperties": true,
          "description": "NodeSelector is a selector which must be true for the pod to fit on a node. Selector which must match a node's labels for the pod to be scheduled on that node.\nSee [nodeSelector in PodSpec of Pod](https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/#scheduling)\n@section -- Deployment",
          "title": "nodeSelector"
        },
        "podAnnotations": {
          "additionalProperties": true,
          "description": "Annotations is an unstructured key value map stored with a resource that may be set by external tools to store and retrieve arbitrary metadata.\nSee [annotations of ObjectMeta](https://kubernetes.io/docs/reference/kubernetes-api/common-definitions/object-meta/#ObjectMeta)\n@section -- Deployment",
          "title": "podAnnotations"
        },
        "podLabels": {
          "additionalProperties": true,
          "description": "Map of string keys and values that can be used to organize and categorize (scope and select) objects. \nSee [labels of ObjectMeta](https://kubernetes.io/docs/reference/kubernetes-api/common-definitions/object-meta/#ObjectMeta)\n@section -- Deployment",
          "title": "podLabels"
        },
        "replicaCount": {
          "default": 1,
          "description": "Number of desired pods. \nSee [replicas in DeploymentSpec of Deployment](https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/deployment-v1/#DeploymentSpec)\n@section -- Deployment",
          "title": "replicaCount",
          "type": "integer"
        },
        "tolerations": {
          "items": {},
          "description": "If specified, the pod's tolerations.\nSee [tolerations in PodSpec of Pod](https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/#scheduling)\n@section -- Deployment",
          "title": "tolerations",
          "type": "array"
        }
      },
      "title": "deployment",
      "type": "object",
      "required": [
        "replicaCount",
        "image",
        "tolerations"
      ]
    },
    "fullnameOverride": {
      "default": "",
      "description": "Custom full name for the deployment.\n@section -- Helm",
      "title": "fullnameOverride",
      "type": "string"
    },
    "global": {
      "description": "Global values are values that can be accessed from any chart or subchart by exactly the same name.",
      "title": "global",
      "type": "object"
    },
    "ingress": {
      "additionalProperties": false,
      "properties": {
        "annotations": {
          "additionalProperties": true,
          "description": "Annotations is an unstructured key value map stored with a resource that may be set by external tools to store and retrieve arbitrary metadata.\nSee [annotations of ObjectMeta](https://kubernetes.io/docs/reference/kubernetes-api/common-definitions/object-meta/#ObjectMeta)\n@section -- Ingress",
          "title": "annotations"
        },
        "className": {
          "default": "",
          "description": "The name of an IngressClass cluster resource.\nSee [ingressClassName in IngressSpec of Ingress](https://kubernetes.io/docs/reference/kubernetes-api/service-resources/ingress-v1/#IngressSpec)\n@section -- Ingress",
          "title": "className",
          "type": "string"
        },
        "enabled": {
          "default": false,
          "description": "Enable Ingress. Ingress is a collection of rules that allow inbound connections to reach the endpoints defined by a backend.\nSee [Ingress](https://kubernetes.io/docs/reference/kubernetes-api/service-resources/ingress-v1/)\n@section -- Ingress",
          "title": "enabled",
          "type": "boolean"
        },
        "hosts": {
          "items": {
            "anyOf": [
              {
                "additionalProperties": false,
                "properties": {
                  "host": {
                    "default": "marty-media-server.local",
                    "title": "host",
                    "type": "string"
                  },
                  "paths": {
                    "items": {
                      "anyOf": [
                        {
                          "additionalProperties": false,
                          "properties": {
                            "path": {
                              "default": "/",
                              "title": "path",
                              "type": "string"
                            },
                            "pathType": {
                              "default": "ImplementationSpecific",
                              "title": "pathType",
                              "type": "string"
                            }
                          },
                          "type": "object",
                          "required": [
                            "path",
                            "pathType"
                          ]
                        }
                      ]
                    },
                    "title": "paths",
                    "type": "array"
                  }
                },
                "type": "object",
                "required": [
                  "host",
                  "paths"
                ]
              }
            ]
          },
          "description": "A list of host rules used to configure the Ingress.\nSee [rules in IngressSpec of Ingress](https://kubernetes.io/docs/reference/kubernetes-api/service-resources/ingress-v1/#IngressSpec)\n@section -- Ingress",
          "title": "hosts",
          "type": "array"
        },
        "tls": {
          "items": {},
          "description": "Represents the TLS configuration. If multiple members of this list specify different hosts, they will be multiplexed on the same port according to the hostname specified through the SNI TLS extension, if the ingress controller fulfilling the ingress supports SNI.\nSee [tls in IngressSpec of Ingress](https://kubernetes.io/docs/reference/kubernetes-api/service-resources/ingress-v1/#IngressSpec)\n@section -- Ingress",
          "title": "tls",
          "type": "array"
        }
      },
      "title": "ingress",
      "type": "object",
      "required": [
        "enabled",
        "className",
        "hosts",
        "tls"
      ]
    },
    "livenessProbe": {
      "additionalProperties": false,
      "properties": {
        "httpGet": {
          "additionalProperties": false,
          "properties": {
            "path": {
              "default": "/",
              "title": "path",
              "type": "string"
            },
            "port": {
              "default": "http",
              "title": "port",
              "type": "string"
            }
          },
          "title": "httpGet",
          "type": "object",
          "required": [
            "path",
            "port"
          ]
        }
      },
      "title": "livenessProbe",
      "type": "object",
      "required": [
        "httpGet"
      ]
    },
    "nameOverride": {
      "default": "",
      "description": "Custom name for this app deployment.\n@section -- Helm",
      "title": "nameOverride",
      "type": "string"
    },
    "podSecurityContext": {
      "additionalProperties": false,
      "title": "podSecurityContext",
      "type": "object"
    },
    "readinessProbe": {
      "additionalProperties": false,
      "properties": {
        "httpGet": {
          "additionalProperties": false,
          "properties": {
            "path": {
              "default": "/",
              "title": "path",
              "type": "string"
            },
            "port": {
              "default": "http",
              "title": "port",
              "type": "string"
            }
          },
          "title": "httpGet",
          "type": "object",
          "required": [
            "path",
            "port"
          ]
        }
      },
      "title": "readinessProbe",
      "type": "object",
      "required": [
        "httpGet"
      ]
    },
    "resources": {
      "additionalProperties": false,
      "title": "resources",
      "type": "object"
    },
    "securityContext": {
      "additionalProperties": false,
      "title": "securityContext",
      "type": "object"
    },
    "service": {
      "additionalProperties": false,
      "properties": {
        "port": {
          "default": 80,
          "description": "The port that will be exposed by this service.\nSee [ports.port in ServiceSpec of Service](https://kubernetes.io/docs/reference/kubernetes-api/service-resources/service-v1/#ServiceSpec)\n@section -- Service",
          "title": "port",
          "type": "integer"
        },
        "type": {
          "default": "ClusterIP",
          "description": "Determines how the Service is exposed.\nSee [type in ServiceSpec of Service](https://kubernetes.io/docs/reference/kubernetes-api/service-resources/service-v1/#ServiceSpec)\n@section -- Service",
          "title": "type",
          "type": "string"
        }
      },
      "title": "service",
      "type": "object",
      "required": [
        "type",
        "port"
      ]
    },
    "serviceAccount": {
      "additionalProperties": false,
      "properties": {
        "annotations": {
          "additionalProperties": false,
          "description": "Annotations to add to the service account",
          "title": "annotations",
          "type": "object"
        },
        "automount": {
          "default": true,
          "description": "Automatically mount a ServiceAccount's API credentials?",
          "title": "automount",
          "type": "boolean"
        },
        "create": {
          "default": true,
          "description": "Specifies whether a service account should be created",
          "title": "create",
          "type": "boolean"
        },
        "name": {
          "default": "",
          "description": "The name of the service account to use.\nIf not set and create is true, a name is generated using the fullname template",
          "title": "name",
          "type": "string"
        }
      },
      "title": "serviceAccount",
      "type": "object",
      "required": [
        "create",
        "automount",
        "annotations",
        "name"
      ]
    },
    "volumeMounts": {
      "items": {},
      "description": "Additional volumeMounts on the output Deployment definition.",
      "title": "volumeMounts",
      "type": "array"
    },
    "volumes": {
      "items": {},
      "description": "Additional volumes on the output Deployment definition.",
      "title": "volumes",
      "type": "array"
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "type": "object",
  "required": [
    "config",
    "database",
    "nameOverride",
    "fullnameOverride",
    "deployment",
    "service",
    "ingress",
    "autoscaling",
    "serviceAccount",
    "podSecurityContext",
    "securityContext",
    "resources",
    "livenessProbe",
    "readinessProbe",
    "volumes",
    "volumeMounts"
  ]
}