{{/*
Expand the name of the chart.
*/}}
{{- define "marty-media-server.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "marty-media-server.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "marty-media-server.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "marty-media-server.labels" -}}
helm.sh/chart: {{ include "marty-media-server.chart" . }}
{{ include "marty-media-server.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "marty-media-server.selectorLabels" -}}
app.kubernetes.io/name: {{ include "marty-media-server.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "marty-media-server.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "marty-media-server.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Name for the Cloud Native PG database.
*/}}
{{- define "marty-media-server.postgresqlcnpg.fullname" -}}
{{- $name := (include "marty-media-server.fullname" .) }}
{{- printf "%s-%s" ($name | trunc 57 | trimSuffix "-") "db" }}
{{- end }}

{{/*
Name for the Cloud Native PG database secret.
*/}}
{{- define "marty-media-server.postgresqlcnpg.secret" -}}
{{- $name := (include "marty-media-server.postgresqlcnpg.fullname" .) }}
{{- printf "%s-%s" $name "app" }}
{{- end }}
