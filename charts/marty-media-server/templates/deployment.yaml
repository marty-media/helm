apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "marty-media-server.fullname" . }}
  labels:
    {{- include "marty-media-server.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "marty-media-server.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      {{- with .Values.deployment.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "marty-media-server.labels" . | nindent 8 }}
        {{- with .Values.deployment.podLabels }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
    spec:
      {{- with .Values.deployment.image.pullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "marty-media-server.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.deployment.image.repository }}:{{ .Values.deployment.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.deployment.image.pullPolicy }}
          ports:
            - name: http
              containerPort: 43200
              protocol: TCP
          env:
            {{- with .Values.config.storage.tempDir }}
            - name: MMS_STORAGE_TEMP_DIR
              value: {{ . | quote }}
            {{- end }}
            {{- with .Values.config.storage.cacheSize }}
            - name: MMS_STORAGE_CACHE_SIZE
              value: {{ . | int64 | quote }}
            {{- end }}
            {{- if eq .Values.database.type "postgresqlcredentials" }}
            - name: MMS_DATABASE_POSTGRES_ADDRESS
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.database.postgresqlcredentials.address }}
                  key: {{ .Values.database.postgresqlcredentials.addressKey }}
            - name: MMS_DATABASE_POSTGRES_PORT
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.database.postgresqlcredentials.port }}
                  key: {{ .Values.database.postgresqlcredentials.portKey }}
            - name: MMS_DATABASE_POSTGRES_USER
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.database.postgresqlcredentials.user }}
                  key: {{ .Values.database.postgresqlcredentials.userKey }}
            - name: MMS_DATABASE_POSTGRES_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.database.postgresqlcredentials.password }}
                  key: {{ .Values.database.postgresqlcredentials.passwordKey }}
            - name: MMS_DATABASE_POSTGRES_DATABASE
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.database.postgresqlcredentials.database }}
                  key: {{ .Values.database.postgresqlcredentials.databaseKey }}
            {{- else if eq .Values.database.type "postgresqlcnpg" }}
            - name: MMS_DATABASE_POSTGRES_ADDRESS
              valueFrom:
                secretKeyRef:
                  name: {{ include "marty-media-server.postgresqlcnpg.secret" . }}
                  key: host
            - name: MMS_DATABASE_POSTGRES_PORT
              valueFrom:
                secretKeyRef:
                  name: {{ include "marty-media-server.postgresqlcnpg.secret" . }}
                  key: port
            - name: MMS_DATABASE_POSTGRES_USER
              valueFrom:
                secretKeyRef:
                  name: {{ include "marty-media-server.postgresqlcnpg.secret" . }}
                  key: user
            - name: MMS_DATABASE_POSTGRES_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ include "marty-media-server.postgresqlcnpg.secret" . }}
                  key: password
            - name: MMS_DATABASE_POSTGRES_DATABASE
              valueFrom:
                secretKeyRef:
                  name: {{ include "marty-media-server.postgresqlcnpg.secret" . }}
                  key: dbname
            {{- else }}
            {{- fail "missing/invalid database type" }}
            {{- end }}
          livenessProbe:
            {{- toYaml .Values.livenessProbe | nindent 12 }}
          readinessProbe:
            {{- toYaml .Values.readinessProbe | nindent 12 }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
          volumeMounts:
            {{- toYaml .Values.volumeMounts | nindent 12 }}
      volumes:
        {{- toYaml .Values.volumes | nindent 8 }}
      {{- with .Values.deployment.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.deployment.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.deployment.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
